apiVersion: 1.0.0
metadata:
  name: python-plugin-test
projects:
  - name: python-hello-world
    source:
      location: 'https://github.com/che-samples/python-hello-world.git'
      type: git
      branch: che-qe-tests
components:
  - id: ms-python/python/latest
    type: chePlugin